package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    public String salle;
    public String poste;
    public String DISTANCIEL;


    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        Spinner spinSalles = view.findViewById(R.id.spSalle);
        spinSalles.setAdapter(ArrayAdapter.createFromResource(getContext(),R.array.list_salles,0));

        Spinner spinPostes = view.findViewById(R.id.spPoste);
        spinPostes.setAdapter(ArrayAdapter.createFromResource(getContext(),R.array.list_postes,0));

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText tvLogin = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    // TODO Q9
}